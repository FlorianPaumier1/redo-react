import Component from "../React/Component.js";
import Header from "./Header.js";
import HomeContent from "./HomeContent.js";
import Footer from "./Footer.js";

export default class Home extends Component {

    constructor() {
        super();
    }

    render() {
        return this.dom.createHtmlElement("div", {id: "Id"},
            this.dom.createHtmlElement(Header, {id: "Header"}),
            this.dom.createHtmlElement(HomeContent, {class: "red", src: ""}),
            this.dom.createHtmlElement(Footer, null),
            );
    }
}
