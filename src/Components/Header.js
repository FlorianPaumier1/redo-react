import Component from "../React/Component.js";

export default class Header extends Component {
    constructor() {
        super();
    }

    render(){
        return this.dom.createHtmlElement("header", {"id" : this.state.id},
            "Je suis le {{ state.id }}",
            this.dom.createHtmlElement("button", {"click": () => this.dom.react.router.push("/url2"), "class" : 'link'}, "Click")
        )
    }
}
