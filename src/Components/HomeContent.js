import Component from "../React/Component.js";

export default class HomeContent extends Component {
    constructor(props) {
        super(props);
    }

    getImage = () => {
        fetch('https://loremflickr.com/500/600/animals')
        .then(function(response) {
            return response.blob();
        })
        .then((myBlob) => {
            const objectURL = URL.createObjectURL(myBlob);
            this.setState({...this.state, src: objectURL});
        });
    }

    render(){
        return this.dom.createHtmlElement("div", {class: this.state.class}, "Hello World",
            this.dom.createHtmlElement("button", {"click": () => this.getImage() }, "Generate image"),
            this.dom.createHtmlElement("img", {"src": this.state.src }),
        )
    }
}
