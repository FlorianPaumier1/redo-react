import {type_check_v2} from "./Helpers.js";

export default class Component {

    /**
     * @private {object}
     */
    _state = {};

    /**
     * @private {object}
     */
    _prevState = {};

    /**
     * @private {Dom}
     */
    _dom

    _rendered
    _oldComp

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = {...value};
    }

    get prevState() {
        return this._prevState;
    }

    set prevState(value) {
        this._prevState = {...value};
    }

    get dom() {
        return this._dom;
    }

    set dom(value) {
        this._dom = value;
    }

    get rendered() {
        return this._rendered;
    }

    set rendered(value) {
        this._rendered = value;
    }

    constructor() {
        this._state = null;
        this._prevState = null;
    }

    receiveData(data) {
        if (data && data.type_check( this.propTypes)) this.state = data;
    }

    setState(state) {
        this.prevState = {...this.state};
        this.state = {...state};

        if(this.needUpdate()) {
            let oldRendered = this.rendered
            this.rendered = this.render();
            this.dom.updateDom(this, oldRendered);
        }
    }

    needUpdate = () => !type_check_v2(this._state, {"value": this._prevState});
}
