import {type_check_v2} from "../Helpers.js";

Object.prototype.type_check = function (types) {
    let arg = this

    let isChecked = type_check_v2(arg, types);

    if (!types.properties) return isChecked;

    for (const typeKey in types.properties) {
        isChecked = type_check(
            type_check_v1(arg, 'object') ? arg[typeKey] : arg
            , types.properties[typeKey]
        );
        if (!isChecked) break;
    }

    return isChecked;
}

Object.prototype.prop_access = function (props) {
    let obj = this;

    if (obj === "undefined" || !obj) return obj;
    if (typeof props !== "string" || !props) return obj;

    let propPath = "";

    for (let accessKey of props.split(".")) {
        accessKey = accessKey.trim();
        propPath += propPath.length > 0 ? `.${accessKey}` : accessKey;

        if (!obj[accessKey]) throw new Error(`${propPath} don't exist`);
        obj = obj[accessKey];
    }

    return obj;
}

Object.prototype.propTypes = {}
