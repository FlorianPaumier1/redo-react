String.prototype.interpolate = function (object){
    let string = this.valueOf()

    const regex = /.*?{{(.*?)}}.*?/gm;
    let m;
    let match = [];

    while ((m = regex.exec(string)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        match.push(m[1]);
    }

    match.forEach(path => {
        string = string.replace("{{" + path + "}}", object.prop_access(path));
    })

    return string;
}
