export const type_check = function(variable, type){
	switch(typeof variable){
		case "symbol":
		case "number":
		case "string":
		case "boolean":
		case "undefined":
		case "function":
			return type === typeof variable
		case "object":
			switch(type){
				case "null":
					return variable === null;
				case "array":
					return Array.isArray(variable);
				default:
					return variable !== null && !Array.isArray(variable);
			}
	}
}

export const type_check_v2 = function(variable , conf){
	for(let toCheck in conf){
		switch(toCheck){
			case "type":
				if(type_check_v1(variable , conf.type) === false) return false;
				break;
			case "value":
				if (JSON.stringify(variable, refReplacer()) !== JSON.stringify(conf.value, refReplacer())) return false;
				break;
			case "enum":
				let found = false;
				for(let subValue of conf.enum){
					found = type_check_v2(variable, {value: subValue});
					if(found)break;
				}
				if(!found) return false;
				break;
		}
	}
	return true;
}

export const type_check_v3 = function(arg, types){
	const check = type_check_v2(arg, types);
    if (!types.properties) return check;
    for (const tk in types.properties){
        let propCheck = type_check_v3(type_check(arg, 'object') ? arg[tk] : arg, types.properties[tk]);
        if (!propCheck) return propCheck;
    }
	return true;
}
export const events = ["click", "hover", 'blur', 'focus', 'change', 'close', "open", "scroll", 'touch']

export function refReplacer() {
	let m = new Map(), v= new Map(), init = null;

	return function(field, value) {
		let p= m.get(this) + (Array.isArray(this) ? `[${field}]` : '.' + field);
		let isComplex= value===Object(value)

		if (isComplex) m.set(value, p);

		let pp = v.get(value)||'';
		let path = p.replace(/undefined\.\.?/,'');
		let val = pp ? `#REF:${pp[0]=='[' ? '$':'$.'}${pp}` : value;

		!init ? (init=value) : (val===init ? val="#REF:$" : 0);
		if(!pp && isComplex) v.set(value, path);

		return val;
	}
}

export function isClass(func) {
	return typeof func === 'function'
		&& /^class\s/.test(Function.prototype.toString.call(func));
}
